package com.liferaydev.labs;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.liferaydev.labs.model.ProductRegister;
import com.liferaydev.labs.service.ProductRegisterLocalService;
import com.liferaydev.labs.service.ProductRegisterLocalServiceUtil;
import com.liferaydev.labs.service.ProductRegistrationLocalServiceUtil;

public class ProductRegistrationController extends MVCPortlet {
	 public void registerProduct(ActionRequest request, ActionResponse response) throws PortalException, SystemException{
		 			System.out.println(ParamUtil.getString(request, "direccion"));
		 			
		 			ServiceContext serviceContext = ServiceContextFactory.getInstance(ProductRegister.class.getName(), request);
		 			
		 			String nombre = ParamUtil.getString(request, "nombre");
		 			String direccion = ParamUtil.getString(request, "direccion");
		 					 	
		 			
		 			ProductRegisterLocalServiceUtil.addProduct(serviceContext, nombre, direccion);
	 }
}
 