/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.liferaydev.labs.model.ProductRegister;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ProductRegister in entity cache.
 *
 * @author esanmiguel
 * @see ProductRegister
 * @generated
 */
public class ProductRegisterCacheModel implements CacheModel<ProductRegister>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{name=");
		sb.append(name);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", direccion=");
		sb.append(direccion);
		sb.append(", telefono=");
		sb.append(telefono);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProductRegister toEntityModel() {
		ProductRegisterImpl productRegisterImpl = new ProductRegisterImpl();

		productRegisterImpl.setName(name);
		productRegisterImpl.setGroupId(groupId);
		productRegisterImpl.setCompanyId(companyId);
		productRegisterImpl.setUserId(userId);

		if (userName == null) {
			productRegisterImpl.setUserName(StringPool.BLANK);
		}
		else {
			productRegisterImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			productRegisterImpl.setCreateDate(null);
		}
		else {
			productRegisterImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			productRegisterImpl.setModifiedDate(null);
		}
		else {
			productRegisterImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (nombre == null) {
			productRegisterImpl.setNombre(StringPool.BLANK);
		}
		else {
			productRegisterImpl.setNombre(nombre);
		}

		if (direccion == null) {
			productRegisterImpl.setDireccion(StringPool.BLANK);
		}
		else {
			productRegisterImpl.setDireccion(direccion);
		}

		if (telefono == null) {
			productRegisterImpl.setTelefono(StringPool.BLANK);
		}
		else {
			productRegisterImpl.setTelefono(telefono);
		}

		productRegisterImpl.resetOriginalValues();

		return productRegisterImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		name = objectInput.readLong();
		groupId = objectInput.readLong();
		companyId = objectInput.readLong();
		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		nombre = objectInput.readUTF();
		direccion = objectInput.readUTF();
		telefono = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(name);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (nombre == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		if (direccion == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(direccion);
		}

		if (telefono == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(telefono);
		}
	}

	public long name;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String nombre;
	public String direccion;
	public String telefono;
}