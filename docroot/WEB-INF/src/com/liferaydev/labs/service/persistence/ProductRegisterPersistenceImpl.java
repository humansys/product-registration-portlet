/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.liferaydev.labs.NoSuchProductRegisterException;
import com.liferaydev.labs.model.ProductRegister;
import com.liferaydev.labs.model.impl.ProductRegisterImpl;
import com.liferaydev.labs.model.impl.ProductRegisterModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the product register service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author esanmiguel
 * @see ProductRegisterPersistence
 * @see ProductRegisterUtil
 * @generated
 */
public class ProductRegisterPersistenceImpl extends BasePersistenceImpl<ProductRegister>
	implements ProductRegisterPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProductRegisterUtil} to access the product register persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProductRegisterImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterModelImpl.FINDER_CACHE_ENABLED,
			ProductRegisterImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterModelImpl.FINDER_CACHE_ENABLED,
			ProductRegisterImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PRODUCTOPORNOMBRE =
		new FinderPath(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterModelImpl.FINDER_CACHE_ENABLED,
			ProductRegisterImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByProductoPorNombre",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTOPORNOMBRE =
		new FinderPath(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterModelImpl.FINDER_CACHE_ENABLED,
			ProductRegisterImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByProductoPorNombre", new String[] { String.class.getName() },
			ProductRegisterModelImpl.NOMBRE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PRODUCTOPORNOMBRE = new FinderPath(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProductoPorNombre", new String[] { String.class.getName() });

	/**
	 * Returns all the product registers where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching product registers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductRegister> findByProductoPorNombre(String nombre)
		throws SystemException {
		return findByProductoPorNombre(nombre, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the product registers where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of product registers
	 * @param end the upper bound of the range of product registers (not inclusive)
	 * @return the range of matching product registers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductRegister> findByProductoPorNombre(String nombre,
		int start, int end) throws SystemException {
		return findByProductoPorNombre(nombre, start, end, null);
	}

	/**
	 * Returns an ordered range of all the product registers where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of product registers
	 * @param end the upper bound of the range of product registers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching product registers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductRegister> findByProductoPorNombre(String nombre,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTOPORNOMBRE;
			finderArgs = new Object[] { nombre };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PRODUCTOPORNOMBRE;
			finderArgs = new Object[] { nombre, start, end, orderByComparator };
		}

		List<ProductRegister> list = (List<ProductRegister>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (ProductRegister productRegister : list) {
				if (!Validator.equals(nombre, productRegister.getNombre())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PRODUCTREGISTER_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProductRegisterModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				if (!pagination) {
					list = (List<ProductRegister>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductRegister>(list);
				}
				else {
					list = (List<ProductRegister>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first product register in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product register
	 * @throws com.liferaydev.labs.NoSuchProductRegisterException if a matching product register could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister findByProductoPorNombre_First(String nombre,
		OrderByComparator orderByComparator)
		throws NoSuchProductRegisterException, SystemException {
		ProductRegister productRegister = fetchByProductoPorNombre_First(nombre,
				orderByComparator);

		if (productRegister != null) {
			return productRegister;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductRegisterException(msg.toString());
	}

	/**
	 * Returns the first product register in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching product register, or <code>null</code> if a matching product register could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister fetchByProductoPorNombre_First(String nombre,
		OrderByComparator orderByComparator) throws SystemException {
		List<ProductRegister> list = findByProductoPorNombre(nombre, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last product register in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product register
	 * @throws com.liferaydev.labs.NoSuchProductRegisterException if a matching product register could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister findByProductoPorNombre_Last(String nombre,
		OrderByComparator orderByComparator)
		throws NoSuchProductRegisterException, SystemException {
		ProductRegister productRegister = fetchByProductoPorNombre_Last(nombre,
				orderByComparator);

		if (productRegister != null) {
			return productRegister;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProductRegisterException(msg.toString());
	}

	/**
	 * Returns the last product register in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching product register, or <code>null</code> if a matching product register could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister fetchByProductoPorNombre_Last(String nombre,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByProductoPorNombre(nombre);

		if (count == 0) {
			return null;
		}

		List<ProductRegister> list = findByProductoPorNombre(nombre, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the product registers before and after the current product register in the ordered set where nombre = &#63;.
	 *
	 * @param name the primary key of the current product register
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next product register
	 * @throws com.liferaydev.labs.NoSuchProductRegisterException if a product register with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister[] findByProductoPorNombre_PrevAndNext(long name,
		String nombre, OrderByComparator orderByComparator)
		throws NoSuchProductRegisterException, SystemException {
		ProductRegister productRegister = findByPrimaryKey(name);

		Session session = null;

		try {
			session = openSession();

			ProductRegister[] array = new ProductRegisterImpl[3];

			array[0] = getByProductoPorNombre_PrevAndNext(session,
					productRegister, nombre, orderByComparator, true);

			array[1] = productRegister;

			array[2] = getByProductoPorNombre_PrevAndNext(session,
					productRegister, nombre, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProductRegister getByProductoPorNombre_PrevAndNext(
		Session session, ProductRegister productRegister, String nombre,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PRODUCTREGISTER_WHERE);

		boolean bindNombre = false;

		if (nombre == null) {
			query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_1);
		}
		else if (nombre.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_3);
		}
		else {
			bindNombre = true;

			query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProductRegisterModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNombre) {
			qPos.add(nombre);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(productRegister);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProductRegister> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the product registers where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByProductoPorNombre(String nombre)
		throws SystemException {
		for (ProductRegister productRegister : findByProductoPorNombre(nombre,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(productRegister);
		}
	}

	/**
	 * Returns the number of product registers where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching product registers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByProductoPorNombre(String nombre)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PRODUCTOPORNOMBRE;

		Object[] finderArgs = new Object[] { nombre };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PRODUCTREGISTER_WHERE);

			boolean bindNombre = false;

			if (nombre == null) {
				query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_1);
			}
			else if (nombre.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(nombre);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_1 = "productRegister.nombre IS NULL";
	private static final String _FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_2 = "productRegister.nombre = ?";
	private static final String _FINDER_COLUMN_PRODUCTOPORNOMBRE_NOMBRE_3 = "(productRegister.nombre IS NULL OR productRegister.nombre = '')";

	public ProductRegisterPersistenceImpl() {
		setModelClass(ProductRegister.class);
	}

	/**
	 * Caches the product register in the entity cache if it is enabled.
	 *
	 * @param productRegister the product register
	 */
	@Override
	public void cacheResult(ProductRegister productRegister) {
		EntityCacheUtil.putResult(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterImpl.class, productRegister.getPrimaryKey(),
			productRegister);

		productRegister.resetOriginalValues();
	}

	/**
	 * Caches the product registers in the entity cache if it is enabled.
	 *
	 * @param productRegisters the product registers
	 */
	@Override
	public void cacheResult(List<ProductRegister> productRegisters) {
		for (ProductRegister productRegister : productRegisters) {
			if (EntityCacheUtil.getResult(
						ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
						ProductRegisterImpl.class,
						productRegister.getPrimaryKey()) == null) {
				cacheResult(productRegister);
			}
			else {
				productRegister.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all product registers.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(ProductRegisterImpl.class.getName());
		}

		EntityCacheUtil.clearCache(ProductRegisterImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the product register.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProductRegister productRegister) {
		EntityCacheUtil.removeResult(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterImpl.class, productRegister.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ProductRegister> productRegisters) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProductRegister productRegister : productRegisters) {
			EntityCacheUtil.removeResult(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
				ProductRegisterImpl.class, productRegister.getPrimaryKey());
		}
	}

	/**
	 * Creates a new product register with the primary key. Does not add the product register to the database.
	 *
	 * @param name the primary key for the new product register
	 * @return the new product register
	 */
	@Override
	public ProductRegister create(long name) {
		ProductRegister productRegister = new ProductRegisterImpl();

		productRegister.setNew(true);
		productRegister.setPrimaryKey(name);

		return productRegister;
	}

	/**
	 * Removes the product register with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param name the primary key of the product register
	 * @return the product register that was removed
	 * @throws com.liferaydev.labs.NoSuchProductRegisterException if a product register with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister remove(long name)
		throws NoSuchProductRegisterException, SystemException {
		return remove((Serializable)name);
	}

	/**
	 * Removes the product register with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the product register
	 * @return the product register that was removed
	 * @throws com.liferaydev.labs.NoSuchProductRegisterException if a product register with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister remove(Serializable primaryKey)
		throws NoSuchProductRegisterException, SystemException {
		Session session = null;

		try {
			session = openSession();

			ProductRegister productRegister = (ProductRegister)session.get(ProductRegisterImpl.class,
					primaryKey);

			if (productRegister == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProductRegisterException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(productRegister);
		}
		catch (NoSuchProductRegisterException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProductRegister removeImpl(ProductRegister productRegister)
		throws SystemException {
		productRegister = toUnwrappedModel(productRegister);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(productRegister)) {
				productRegister = (ProductRegister)session.get(ProductRegisterImpl.class,
						productRegister.getPrimaryKeyObj());
			}

			if (productRegister != null) {
				session.delete(productRegister);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (productRegister != null) {
			clearCache(productRegister);
		}

		return productRegister;
	}

	@Override
	public ProductRegister updateImpl(
		com.liferaydev.labs.model.ProductRegister productRegister)
		throws SystemException {
		productRegister = toUnwrappedModel(productRegister);

		boolean isNew = productRegister.isNew();

		ProductRegisterModelImpl productRegisterModelImpl = (ProductRegisterModelImpl)productRegister;

		Session session = null;

		try {
			session = openSession();

			if (productRegister.isNew()) {
				session.save(productRegister);

				productRegister.setNew(false);
			}
			else {
				session.merge(productRegister);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProductRegisterModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((productRegisterModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTOPORNOMBRE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						productRegisterModelImpl.getOriginalNombre()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRODUCTOPORNOMBRE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTOPORNOMBRE,
					args);

				args = new Object[] { productRegisterModelImpl.getNombre() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PRODUCTOPORNOMBRE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PRODUCTOPORNOMBRE,
					args);
			}
		}

		EntityCacheUtil.putResult(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
			ProductRegisterImpl.class, productRegister.getPrimaryKey(),
			productRegister);

		return productRegister;
	}

	protected ProductRegister toUnwrappedModel(ProductRegister productRegister) {
		if (productRegister instanceof ProductRegisterImpl) {
			return productRegister;
		}

		ProductRegisterImpl productRegisterImpl = new ProductRegisterImpl();

		productRegisterImpl.setNew(productRegister.isNew());
		productRegisterImpl.setPrimaryKey(productRegister.getPrimaryKey());

		productRegisterImpl.setName(productRegister.getName());
		productRegisterImpl.setGroupId(productRegister.getGroupId());
		productRegisterImpl.setCompanyId(productRegister.getCompanyId());
		productRegisterImpl.setUserId(productRegister.getUserId());
		productRegisterImpl.setUserName(productRegister.getUserName());
		productRegisterImpl.setCreateDate(productRegister.getCreateDate());
		productRegisterImpl.setModifiedDate(productRegister.getModifiedDate());
		productRegisterImpl.setNombre(productRegister.getNombre());
		productRegisterImpl.setDireccion(productRegister.getDireccion());
		productRegisterImpl.setTelefono(productRegister.getTelefono());

		return productRegisterImpl;
	}

	/**
	 * Returns the product register with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the product register
	 * @return the product register
	 * @throws com.liferaydev.labs.NoSuchProductRegisterException if a product register with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProductRegisterException, SystemException {
		ProductRegister productRegister = fetchByPrimaryKey(primaryKey);

		if (productRegister == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProductRegisterException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return productRegister;
	}

	/**
	 * Returns the product register with the primary key or throws a {@link com.liferaydev.labs.NoSuchProductRegisterException} if it could not be found.
	 *
	 * @param name the primary key of the product register
	 * @return the product register
	 * @throws com.liferaydev.labs.NoSuchProductRegisterException if a product register with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister findByPrimaryKey(long name)
		throws NoSuchProductRegisterException, SystemException {
		return findByPrimaryKey((Serializable)name);
	}

	/**
	 * Returns the product register with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the product register
	 * @return the product register, or <code>null</code> if a product register with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		ProductRegister productRegister = (ProductRegister)EntityCacheUtil.getResult(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
				ProductRegisterImpl.class, primaryKey);

		if (productRegister == _nullProductRegister) {
			return null;
		}

		if (productRegister == null) {
			Session session = null;

			try {
				session = openSession();

				productRegister = (ProductRegister)session.get(ProductRegisterImpl.class,
						primaryKey);

				if (productRegister != null) {
					cacheResult(productRegister);
				}
				else {
					EntityCacheUtil.putResult(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
						ProductRegisterImpl.class, primaryKey,
						_nullProductRegister);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(ProductRegisterModelImpl.ENTITY_CACHE_ENABLED,
					ProductRegisterImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return productRegister;
	}

	/**
	 * Returns the product register with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param name the primary key of the product register
	 * @return the product register, or <code>null</code> if a product register with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public ProductRegister fetchByPrimaryKey(long name)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)name);
	}

	/**
	 * Returns all the product registers.
	 *
	 * @return the product registers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductRegister> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the product registers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of product registers
	 * @param end the upper bound of the range of product registers (not inclusive)
	 * @return the range of product registers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductRegister> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the product registers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of product registers
	 * @param end the upper bound of the range of product registers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of product registers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<ProductRegister> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProductRegister> list = (List<ProductRegister>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_PRODUCTREGISTER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PRODUCTREGISTER;

				if (pagination) {
					sql = sql.concat(ProductRegisterModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProductRegister>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<ProductRegister>(list);
				}
				else {
					list = (List<ProductRegister>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the product registers from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (ProductRegister productRegister : findAll()) {
			remove(productRegister);
		}
	}

	/**
	 * Returns the number of product registers.
	 *
	 * @return the number of product registers
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PRODUCTREGISTER);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the product register persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.liferaydev.labs.model.ProductRegister")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<ProductRegister>> listenersList = new ArrayList<ModelListener<ProductRegister>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<ProductRegister>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(ProductRegisterImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_PRODUCTREGISTER = "SELECT productRegister FROM ProductRegister productRegister";
	private static final String _SQL_SELECT_PRODUCTREGISTER_WHERE = "SELECT productRegister FROM ProductRegister productRegister WHERE ";
	private static final String _SQL_COUNT_PRODUCTREGISTER = "SELECT COUNT(productRegister) FROM ProductRegister productRegister";
	private static final String _SQL_COUNT_PRODUCTREGISTER_WHERE = "SELECT COUNT(productRegister) FROM ProductRegister productRegister WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "productRegister.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProductRegister exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProductRegister exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(ProductRegisterPersistenceImpl.class);
	private static ProductRegister _nullProductRegister = new ProductRegisterImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<ProductRegister> toCacheModel() {
				return _nullProductRegisterCacheModel;
			}
		};

	private static CacheModel<ProductRegister> _nullProductRegisterCacheModel = new CacheModel<ProductRegister>() {
			@Override
			public ProductRegister toEntityModel() {
				return _nullProductRegister;
			}
		};
}