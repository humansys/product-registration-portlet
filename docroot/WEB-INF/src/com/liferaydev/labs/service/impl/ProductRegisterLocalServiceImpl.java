/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferaydev.labs.model.ProductRegister;
import com.liferaydev.labs.service.base.ProductRegisterLocalServiceBaseImpl; 

/**
 * The implementation of the product register local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferaydev.labs.service.ProductRegisterLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author esanmiguel
 * @see com.liferaydev.labs.service.base.ProductRegisterLocalServiceBaseImpl
 * @see com.liferaydev.labs.service.ProductRegisterLocalServiceUtil
 */
public class ProductRegisterLocalServiceImpl
	extends ProductRegisterLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferaydev.labs.service.ProductRegisterLocalServiceUtil} to access the product register local service.
	 */
public ProductRegister addProduct(ServiceContext serviceContext, String nombre, String direccion ) throws PortalException, SystemException{
		
		//User user = userPersistence.fetchByPrimaryKey(serviceContext.getUserId()); 
	
		ProductRegister p = null;
		
		try{
			p = productRegisterPersistence.create(counterLocalService.increment());
			p.setNombre(nombre);
			p.setDireccion(direccion);
		}catch (Exception e){
			
		}
		
		
		productRegisterPersistence.update(p);
		
		return p;
	}
}