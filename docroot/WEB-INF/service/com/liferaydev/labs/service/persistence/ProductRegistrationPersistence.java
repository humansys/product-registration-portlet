/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.liferaydev.labs.model.ProductRegistration;

/**
 * The persistence interface for the product registration service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author esanmiguel
 * @see ProductRegistrationPersistenceImpl
 * @see ProductRegistrationUtil
 * @generated
 */
public interface ProductRegistrationPersistence extends BasePersistence<ProductRegistration> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProductRegistrationUtil} to access the product registration persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the product registrations where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the matching product registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferaydev.labs.model.ProductRegistration> findByProductoPorNombre(
		java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the product registrations where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of product registrations
	* @param end the upper bound of the range of product registrations (not inclusive)
	* @return the range of matching product registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferaydev.labs.model.ProductRegistration> findByProductoPorNombre(
		java.lang.String nombre, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the product registrations where nombre = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nombre the nombre
	* @param start the lower bound of the range of product registrations
	* @param end the upper bound of the range of product registrations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching product registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferaydev.labs.model.ProductRegistration> findByProductoPorNombre(
		java.lang.String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first product registration in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching product registration
	* @throws com.liferaydev.labs.NoSuchProductRegistrationException if a matching product registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferaydev.labs.model.ProductRegistration findByProductoPorNombre_First(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferaydev.labs.NoSuchProductRegistrationException;

	/**
	* Returns the first product registration in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching product registration, or <code>null</code> if a matching product registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferaydev.labs.model.ProductRegistration fetchByProductoPorNombre_First(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last product registration in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching product registration
	* @throws com.liferaydev.labs.NoSuchProductRegistrationException if a matching product registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferaydev.labs.model.ProductRegistration findByProductoPorNombre_Last(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferaydev.labs.NoSuchProductRegistrationException;

	/**
	* Returns the last product registration in the ordered set where nombre = &#63;.
	*
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching product registration, or <code>null</code> if a matching product registration could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferaydev.labs.model.ProductRegistration fetchByProductoPorNombre_Last(
		java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the product registrations before and after the current product registration in the ordered set where nombre = &#63;.
	*
	* @param name the primary key of the current product registration
	* @param nombre the nombre
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next product registration
	* @throws com.liferaydev.labs.NoSuchProductRegistrationException if a product registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferaydev.labs.model.ProductRegistration[] findByProductoPorNombre_PrevAndNext(
		long name, java.lang.String nombre,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferaydev.labs.NoSuchProductRegistrationException;

	/**
	* Removes all the product registrations where nombre = &#63; from the database.
	*
	* @param nombre the nombre
	* @throws SystemException if a system exception occurred
	*/
	public void removeByProductoPorNombre(java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of product registrations where nombre = &#63;.
	*
	* @param nombre the nombre
	* @return the number of matching product registrations
	* @throws SystemException if a system exception occurred
	*/
	public int countByProductoPorNombre(java.lang.String nombre)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the product registration in the entity cache if it is enabled.
	*
	* @param productRegistration the product registration
	*/
	public void cacheResult(
		com.liferaydev.labs.model.ProductRegistration productRegistration);

	/**
	* Caches the product registrations in the entity cache if it is enabled.
	*
	* @param productRegistrations the product registrations
	*/
	public void cacheResult(
		java.util.List<com.liferaydev.labs.model.ProductRegistration> productRegistrations);

	/**
	* Creates a new product registration with the primary key. Does not add the product registration to the database.
	*
	* @param name the primary key for the new product registration
	* @return the new product registration
	*/
	public com.liferaydev.labs.model.ProductRegistration create(long name);

	/**
	* Removes the product registration with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param name the primary key of the product registration
	* @return the product registration that was removed
	* @throws com.liferaydev.labs.NoSuchProductRegistrationException if a product registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferaydev.labs.model.ProductRegistration remove(long name)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferaydev.labs.NoSuchProductRegistrationException;

	public com.liferaydev.labs.model.ProductRegistration updateImpl(
		com.liferaydev.labs.model.ProductRegistration productRegistration)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the product registration with the primary key or throws a {@link com.liferaydev.labs.NoSuchProductRegistrationException} if it could not be found.
	*
	* @param name the primary key of the product registration
	* @return the product registration
	* @throws com.liferaydev.labs.NoSuchProductRegistrationException if a product registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferaydev.labs.model.ProductRegistration findByPrimaryKey(
		long name)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferaydev.labs.NoSuchProductRegistrationException;

	/**
	* Returns the product registration with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param name the primary key of the product registration
	* @return the product registration, or <code>null</code> if a product registration with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferaydev.labs.model.ProductRegistration fetchByPrimaryKey(
		long name) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the product registrations.
	*
	* @return the product registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferaydev.labs.model.ProductRegistration> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the product registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of product registrations
	* @param end the upper bound of the range of product registrations (not inclusive)
	* @return the range of product registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferaydev.labs.model.ProductRegistration> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the product registrations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegistrationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of product registrations
	* @param end the upper bound of the range of product registrations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of product registrations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferaydev.labs.model.ProductRegistration> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the product registrations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of product registrations.
	*
	* @return the number of product registrations
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}