/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProductRegisterLocalService}.
 *
 * @author esanmiguel
 * @see ProductRegisterLocalService
 * @generated
 */
public class ProductRegisterLocalServiceWrapper
	implements ProductRegisterLocalService,
		ServiceWrapper<ProductRegisterLocalService> {
	public ProductRegisterLocalServiceWrapper(
		ProductRegisterLocalService productRegisterLocalService) {
		_productRegisterLocalService = productRegisterLocalService;
	}

	/**
	* Adds the product register to the database. Also notifies the appropriate model listeners.
	*
	* @param productRegister the product register
	* @return the product register that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferaydev.labs.model.ProductRegister addProductRegister(
		com.liferaydev.labs.model.ProductRegister productRegister)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.addProductRegister(productRegister);
	}

	/**
	* Creates a new product register with the primary key. Does not add the product register to the database.
	*
	* @param name the primary key for the new product register
	* @return the new product register
	*/
	@Override
	public com.liferaydev.labs.model.ProductRegister createProductRegister(
		long name) {
		return _productRegisterLocalService.createProductRegister(name);
	}

	/**
	* Deletes the product register with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param name the primary key of the product register
	* @return the product register that was removed
	* @throws PortalException if a product register with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferaydev.labs.model.ProductRegister deleteProductRegister(
		long name)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.deleteProductRegister(name);
	}

	/**
	* Deletes the product register from the database. Also notifies the appropriate model listeners.
	*
	* @param productRegister the product register
	* @return the product register that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferaydev.labs.model.ProductRegister deleteProductRegister(
		com.liferaydev.labs.model.ProductRegister productRegister)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.deleteProductRegister(productRegister);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _productRegisterLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public com.liferaydev.labs.model.ProductRegister fetchProductRegister(
		long name) throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.fetchProductRegister(name);
	}

	/**
	* Returns the product register with the primary key.
	*
	* @param name the primary key of the product register
	* @return the product register
	* @throws PortalException if a product register with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferaydev.labs.model.ProductRegister getProductRegister(
		long name)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.getProductRegister(name);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the product registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.liferaydev.labs.model.impl.ProductRegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of product registers
	* @param end the upper bound of the range of product registers (not inclusive)
	* @return the range of product registers
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.liferaydev.labs.model.ProductRegister> getProductRegisters(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.getProductRegisters(start, end);
	}

	/**
	* Returns the number of product registers.
	*
	* @return the number of product registers
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getProductRegistersCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.getProductRegistersCount();
	}

	/**
	* Updates the product register in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param productRegister the product register
	* @return the product register that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.liferaydev.labs.model.ProductRegister updateProductRegister(
		com.liferaydev.labs.model.ProductRegister productRegister)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.updateProductRegister(productRegister);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _productRegisterLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_productRegisterLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _productRegisterLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public com.liferaydev.labs.model.ProductRegister addProduct(
		com.liferay.portal.service.ServiceContext serviceContext,
		java.lang.String nombre, java.lang.String direccion)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _productRegisterLocalService.addProduct(serviceContext, nombre,
			direccion);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public ProductRegisterLocalService getWrappedProductRegisterLocalService() {
		return _productRegisterLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedProductRegisterLocalService(
		ProductRegisterLocalService productRegisterLocalService) {
		_productRegisterLocalService = productRegisterLocalService;
	}

	@Override
	public ProductRegisterLocalService getWrappedService() {
		return _productRegisterLocalService;
	}

	@Override
	public void setWrappedService(
		ProductRegisterLocalService productRegisterLocalService) {
		_productRegisterLocalService = productRegisterLocalService;
	}

	private ProductRegisterLocalService _productRegisterLocalService;
}