/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.liferaydev.labs.service.http.ProductRegisterServiceSoap}.
 *
 * @author esanmiguel
 * @see com.liferaydev.labs.service.http.ProductRegisterServiceSoap
 * @generated
 */
public class ProductRegisterSoap implements Serializable {
	public static ProductRegisterSoap toSoapModel(ProductRegister model) {
		ProductRegisterSoap soapModel = new ProductRegisterSoap();

		soapModel.setName(model.getName());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNombre(model.getNombre());
		soapModel.setDireccion(model.getDireccion());
		soapModel.setTelefono(model.getTelefono());

		return soapModel;
	}

	public static ProductRegisterSoap[] toSoapModels(ProductRegister[] models) {
		ProductRegisterSoap[] soapModels = new ProductRegisterSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProductRegisterSoap[][] toSoapModels(
		ProductRegister[][] models) {
		ProductRegisterSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProductRegisterSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProductRegisterSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProductRegisterSoap[] toSoapModels(
		List<ProductRegister> models) {
		List<ProductRegisterSoap> soapModels = new ArrayList<ProductRegisterSoap>(models.size());

		for (ProductRegister model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProductRegisterSoap[soapModels.size()]);
	}

	public ProductRegisterSoap() {
	}

	public long getPrimaryKey() {
		return _name;
	}

	public void setPrimaryKey(long pk) {
		setName(pk);
	}

	public long getName() {
		return _name;
	}

	public void setName(long name) {
		_name = name;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public String getDireccion() {
		return _direccion;
	}

	public void setDireccion(String direccion) {
		_direccion = direccion;
	}

	public String getTelefono() {
		return _telefono;
	}

	public void setTelefono(String telefono) {
		_telefono = telefono;
	}

	private long _name;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nombre;
	private String _direccion;
	private String _telefono;
}