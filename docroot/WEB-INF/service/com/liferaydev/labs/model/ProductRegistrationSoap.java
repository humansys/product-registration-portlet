/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.liferaydev.labs.service.http.ProductRegistrationServiceSoap}.
 *
 * @author esanmiguel
 * @see com.liferaydev.labs.service.http.ProductRegistrationServiceSoap
 * @generated
 */
public class ProductRegistrationSoap implements Serializable {
	public static ProductRegistrationSoap toSoapModel(ProductRegistration model) {
		ProductRegistrationSoap soapModel = new ProductRegistrationSoap();

		soapModel.setName(model.getName());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNombre(model.getNombre());
		soapModel.setDireccion(model.getDireccion());
		soapModel.setTelefono(model.getTelefono());

		return soapModel;
	}

	public static ProductRegistrationSoap[] toSoapModels(
		ProductRegistration[] models) {
		ProductRegistrationSoap[] soapModels = new ProductRegistrationSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProductRegistrationSoap[][] toSoapModels(
		ProductRegistration[][] models) {
		ProductRegistrationSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProductRegistrationSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProductRegistrationSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProductRegistrationSoap[] toSoapModels(
		List<ProductRegistration> models) {
		List<ProductRegistrationSoap> soapModels = new ArrayList<ProductRegistrationSoap>(models.size());

		for (ProductRegistration model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProductRegistrationSoap[soapModels.size()]);
	}

	public ProductRegistrationSoap() {
	}

	public long getPrimaryKey() {
		return _name;
	}

	public void setPrimaryKey(long pk) {
		setName(pk);
	}

	public long getName() {
		return _name;
	}

	public void setName(long name) {
		_name = name;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public boolean getDireccion() {
		return _direccion;
	}

	public boolean isDireccion() {
		return _direccion;
	}

	public void setDireccion(boolean direccion) {
		_direccion = direccion;
	}

	public int getTelefono() {
		return _telefono;
	}

	public void setTelefono(int telefono) {
		_telefono = telefono;
	}

	private long _name;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nombre;
	private boolean _direccion;
	private int _telefono;
}