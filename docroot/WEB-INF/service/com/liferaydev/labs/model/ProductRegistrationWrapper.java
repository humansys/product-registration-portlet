/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ProductRegistration}.
 * </p>
 *
 * @author esanmiguel
 * @see ProductRegistration
 * @generated
 */
public class ProductRegistrationWrapper implements ProductRegistration,
	ModelWrapper<ProductRegistration> {
	public ProductRegistrationWrapper(ProductRegistration productRegistration) {
		_productRegistration = productRegistration;
	}

	@Override
	public Class<?> getModelClass() {
		return ProductRegistration.class;
	}

	@Override
	public String getModelClassName() {
		return ProductRegistration.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("name", getName());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nombre", getNombre());
		attributes.put("direccion", getDireccion());
		attributes.put("telefono", getTelefono());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long name = (Long)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Boolean direccion = (Boolean)attributes.get("direccion");

		if (direccion != null) {
			setDireccion(direccion);
		}

		Integer telefono = (Integer)attributes.get("telefono");

		if (telefono != null) {
			setTelefono(telefono);
		}
	}

	/**
	* Returns the primary key of this product registration.
	*
	* @return the primary key of this product registration
	*/
	@Override
	public long getPrimaryKey() {
		return _productRegistration.getPrimaryKey();
	}

	/**
	* Sets the primary key of this product registration.
	*
	* @param primaryKey the primary key of this product registration
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_productRegistration.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the name of this product registration.
	*
	* @return the name of this product registration
	*/
	@Override
	public long getName() {
		return _productRegistration.getName();
	}

	/**
	* Sets the name of this product registration.
	*
	* @param name the name of this product registration
	*/
	@Override
	public void setName(long name) {
		_productRegistration.setName(name);
	}

	/**
	* Returns the group ID of this product registration.
	*
	* @return the group ID of this product registration
	*/
	@Override
	public long getGroupId() {
		return _productRegistration.getGroupId();
	}

	/**
	* Sets the group ID of this product registration.
	*
	* @param groupId the group ID of this product registration
	*/
	@Override
	public void setGroupId(long groupId) {
		_productRegistration.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this product registration.
	*
	* @return the company ID of this product registration
	*/
	@Override
	public long getCompanyId() {
		return _productRegistration.getCompanyId();
	}

	/**
	* Sets the company ID of this product registration.
	*
	* @param companyId the company ID of this product registration
	*/
	@Override
	public void setCompanyId(long companyId) {
		_productRegistration.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this product registration.
	*
	* @return the user ID of this product registration
	*/
	@Override
	public long getUserId() {
		return _productRegistration.getUserId();
	}

	/**
	* Sets the user ID of this product registration.
	*
	* @param userId the user ID of this product registration
	*/
	@Override
	public void setUserId(long userId) {
		_productRegistration.setUserId(userId);
	}

	/**
	* Returns the user uuid of this product registration.
	*
	* @return the user uuid of this product registration
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegistration.getUserUuid();
	}

	/**
	* Sets the user uuid of this product registration.
	*
	* @param userUuid the user uuid of this product registration
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_productRegistration.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this product registration.
	*
	* @return the user name of this product registration
	*/
	@Override
	public java.lang.String getUserName() {
		return _productRegistration.getUserName();
	}

	/**
	* Sets the user name of this product registration.
	*
	* @param userName the user name of this product registration
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_productRegistration.setUserName(userName);
	}

	/**
	* Returns the create date of this product registration.
	*
	* @return the create date of this product registration
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _productRegistration.getCreateDate();
	}

	/**
	* Sets the create date of this product registration.
	*
	* @param createDate the create date of this product registration
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_productRegistration.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this product registration.
	*
	* @return the modified date of this product registration
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _productRegistration.getModifiedDate();
	}

	/**
	* Sets the modified date of this product registration.
	*
	* @param modifiedDate the modified date of this product registration
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_productRegistration.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the nombre of this product registration.
	*
	* @return the nombre of this product registration
	*/
	@Override
	public java.lang.String getNombre() {
		return _productRegistration.getNombre();
	}

	/**
	* Sets the nombre of this product registration.
	*
	* @param nombre the nombre of this product registration
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_productRegistration.setNombre(nombre);
	}

	/**
	* Returns the direccion of this product registration.
	*
	* @return the direccion of this product registration
	*/
	@Override
	public boolean getDireccion() {
		return _productRegistration.getDireccion();
	}

	/**
	* Returns <code>true</code> if this product registration is direccion.
	*
	* @return <code>true</code> if this product registration is direccion; <code>false</code> otherwise
	*/
	@Override
	public boolean isDireccion() {
		return _productRegistration.isDireccion();
	}

	/**
	* Sets whether this product registration is direccion.
	*
	* @param direccion the direccion of this product registration
	*/
	@Override
	public void setDireccion(boolean direccion) {
		_productRegistration.setDireccion(direccion);
	}

	/**
	* Returns the telefono of this product registration.
	*
	* @return the telefono of this product registration
	*/
	@Override
	public int getTelefono() {
		return _productRegistration.getTelefono();
	}

	/**
	* Sets the telefono of this product registration.
	*
	* @param telefono the telefono of this product registration
	*/
	@Override
	public void setTelefono(int telefono) {
		_productRegistration.setTelefono(telefono);
	}

	@Override
	public boolean isNew() {
		return _productRegistration.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_productRegistration.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _productRegistration.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_productRegistration.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _productRegistration.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _productRegistration.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_productRegistration.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _productRegistration.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_productRegistration.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_productRegistration.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_productRegistration.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ProductRegistrationWrapper((ProductRegistration)_productRegistration.clone());
	}

	@Override
	public int compareTo(
		com.liferaydev.labs.model.ProductRegistration productRegistration) {
		return _productRegistration.compareTo(productRegistration);
	}

	@Override
	public int hashCode() {
		return _productRegistration.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.liferaydev.labs.model.ProductRegistration> toCacheModel() {
		return _productRegistration.toCacheModel();
	}

	@Override
	public com.liferaydev.labs.model.ProductRegistration toEscapedModel() {
		return new ProductRegistrationWrapper(_productRegistration.toEscapedModel());
	}

	@Override
	public com.liferaydev.labs.model.ProductRegistration toUnescapedModel() {
		return new ProductRegistrationWrapper(_productRegistration.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _productRegistration.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _productRegistration.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_productRegistration.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProductRegistrationWrapper)) {
			return false;
		}

		ProductRegistrationWrapper productRegistrationWrapper = (ProductRegistrationWrapper)obj;

		if (Validator.equals(_productRegistration,
					productRegistrationWrapper._productRegistration)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ProductRegistration getWrappedProductRegistration() {
		return _productRegistration;
	}

	@Override
	public ProductRegistration getWrappedModel() {
		return _productRegistration;
	}

	@Override
	public void resetOriginalValues() {
		_productRegistration.resetOriginalValues();
	}

	private ProductRegistration _productRegistration;
}