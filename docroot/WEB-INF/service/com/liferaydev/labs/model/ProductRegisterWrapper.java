/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferaydev.labs.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ProductRegister}.
 * </p>
 *
 * @author esanmiguel
 * @see ProductRegister
 * @generated
 */
public class ProductRegisterWrapper implements ProductRegister,
	ModelWrapper<ProductRegister> {
	public ProductRegisterWrapper(ProductRegister productRegister) {
		_productRegister = productRegister;
	}

	@Override
	public Class<?> getModelClass() {
		return ProductRegister.class;
	}

	@Override
	public String getModelClassName() {
		return ProductRegister.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("name", getName());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nombre", getNombre());
		attributes.put("direccion", getDireccion());
		attributes.put("telefono", getTelefono());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long name = (Long)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		String direccion = (String)attributes.get("direccion");

		if (direccion != null) {
			setDireccion(direccion);
		}

		String telefono = (String)attributes.get("telefono");

		if (telefono != null) {
			setTelefono(telefono);
		}
	}

	/**
	* Returns the primary key of this product register.
	*
	* @return the primary key of this product register
	*/
	@Override
	public long getPrimaryKey() {
		return _productRegister.getPrimaryKey();
	}

	/**
	* Sets the primary key of this product register.
	*
	* @param primaryKey the primary key of this product register
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_productRegister.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the name of this product register.
	*
	* @return the name of this product register
	*/
	@Override
	public long getName() {
		return _productRegister.getName();
	}

	/**
	* Sets the name of this product register.
	*
	* @param name the name of this product register
	*/
	@Override
	public void setName(long name) {
		_productRegister.setName(name);
	}

	/**
	* Returns the group ID of this product register.
	*
	* @return the group ID of this product register
	*/
	@Override
	public long getGroupId() {
		return _productRegister.getGroupId();
	}

	/**
	* Sets the group ID of this product register.
	*
	* @param groupId the group ID of this product register
	*/
	@Override
	public void setGroupId(long groupId) {
		_productRegister.setGroupId(groupId);
	}

	/**
	* Returns the company ID of this product register.
	*
	* @return the company ID of this product register
	*/
	@Override
	public long getCompanyId() {
		return _productRegister.getCompanyId();
	}

	/**
	* Sets the company ID of this product register.
	*
	* @param companyId the company ID of this product register
	*/
	@Override
	public void setCompanyId(long companyId) {
		_productRegister.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this product register.
	*
	* @return the user ID of this product register
	*/
	@Override
	public long getUserId() {
		return _productRegister.getUserId();
	}

	/**
	* Sets the user ID of this product register.
	*
	* @param userId the user ID of this product register
	*/
	@Override
	public void setUserId(long userId) {
		_productRegister.setUserId(userId);
	}

	/**
	* Returns the user uuid of this product register.
	*
	* @return the user uuid of this product register
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _productRegister.getUserUuid();
	}

	/**
	* Sets the user uuid of this product register.
	*
	* @param userUuid the user uuid of this product register
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_productRegister.setUserUuid(userUuid);
	}

	/**
	* Returns the user name of this product register.
	*
	* @return the user name of this product register
	*/
	@Override
	public java.lang.String getUserName() {
		return _productRegister.getUserName();
	}

	/**
	* Sets the user name of this product register.
	*
	* @param userName the user name of this product register
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_productRegister.setUserName(userName);
	}

	/**
	* Returns the create date of this product register.
	*
	* @return the create date of this product register
	*/
	@Override
	public java.util.Date getCreateDate() {
		return _productRegister.getCreateDate();
	}

	/**
	* Sets the create date of this product register.
	*
	* @param createDate the create date of this product register
	*/
	@Override
	public void setCreateDate(java.util.Date createDate) {
		_productRegister.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this product register.
	*
	* @return the modified date of this product register
	*/
	@Override
	public java.util.Date getModifiedDate() {
		return _productRegister.getModifiedDate();
	}

	/**
	* Sets the modified date of this product register.
	*
	* @param modifiedDate the modified date of this product register
	*/
	@Override
	public void setModifiedDate(java.util.Date modifiedDate) {
		_productRegister.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the nombre of this product register.
	*
	* @return the nombre of this product register
	*/
	@Override
	public java.lang.String getNombre() {
		return _productRegister.getNombre();
	}

	/**
	* Sets the nombre of this product register.
	*
	* @param nombre the nombre of this product register
	*/
	@Override
	public void setNombre(java.lang.String nombre) {
		_productRegister.setNombre(nombre);
	}

	/**
	* Returns the direccion of this product register.
	*
	* @return the direccion of this product register
	*/
	@Override
	public java.lang.String getDireccion() {
		return _productRegister.getDireccion();
	}

	/**
	* Sets the direccion of this product register.
	*
	* @param direccion the direccion of this product register
	*/
	@Override
	public void setDireccion(java.lang.String direccion) {
		_productRegister.setDireccion(direccion);
	}

	/**
	* Returns the telefono of this product register.
	*
	* @return the telefono of this product register
	*/
	@Override
	public java.lang.String getTelefono() {
		return _productRegister.getTelefono();
	}

	/**
	* Sets the telefono of this product register.
	*
	* @param telefono the telefono of this product register
	*/
	@Override
	public void setTelefono(java.lang.String telefono) {
		_productRegister.setTelefono(telefono);
	}

	@Override
	public boolean isNew() {
		return _productRegister.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_productRegister.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _productRegister.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_productRegister.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _productRegister.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _productRegister.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_productRegister.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _productRegister.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_productRegister.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_productRegister.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_productRegister.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new ProductRegisterWrapper((ProductRegister)_productRegister.clone());
	}

	@Override
	public int compareTo(
		com.liferaydev.labs.model.ProductRegister productRegister) {
		return _productRegister.compareTo(productRegister);
	}

	@Override
	public int hashCode() {
		return _productRegister.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.liferaydev.labs.model.ProductRegister> toCacheModel() {
		return _productRegister.toCacheModel();
	}

	@Override
	public com.liferaydev.labs.model.ProductRegister toEscapedModel() {
		return new ProductRegisterWrapper(_productRegister.toEscapedModel());
	}

	@Override
	public com.liferaydev.labs.model.ProductRegister toUnescapedModel() {
		return new ProductRegisterWrapper(_productRegister.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _productRegister.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _productRegister.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_productRegister.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProductRegisterWrapper)) {
			return false;
		}

		ProductRegisterWrapper productRegisterWrapper = (ProductRegisterWrapper)obj;

		if (Validator.equals(_productRegister,
					productRegisterWrapper._productRegister)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public ProductRegister getWrappedProductRegister() {
		return _productRegister;
	}

	@Override
	public ProductRegister getWrappedModel() {
		return _productRegister;
	}

	@Override
	public void resetOriginalValues() {
		_productRegister.resetOriginalValues();
	}

	private ProductRegister _productRegister;
}