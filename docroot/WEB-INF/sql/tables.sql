create table ProductRegistration_ProductRegister (
	name LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	nombre VARCHAR(75) null,
	direccion VARCHAR(75) null,
	telefono VARCHAR(75) null
);

create table ProductRegistration_ProductRegistration (
	name LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	nombre VARCHAR(75) null,
	direccion BOOLEAN,
	telefono INTEGER
);