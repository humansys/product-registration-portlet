<%
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>


<portlet:defineObjects />

<portlet:actionURL name="registerProduct" var="registerProductURL">
</portlet:actionURL>

Este es un portlet para el registro de un producto.

<aui:form action="${registerProductURL}"  method="post">

<aui:input name="nombre" label="Nombre" type="text">
</aui:input>

<aui:input name="direccion" label="Direcci�n" type="textarea">
</aui:input>

<aui:input name="telefono" label="Tel�fono" type="text">
</aui:input>

<aui:button-row>

<aui:button type="submit" value="Enviar"></aui:button>

</aui:button-row>

</aui:form>


